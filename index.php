<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link href="assets/css/style.css" rel="stylesheet">
    <title>Scandiweb task</title>
</head>

<?php

require "vendor/autoload.php";

$product = new TaskController();
$product_data = $product->getAll();


if (isset($_POST['delete_arr'])) {
    if (!empty($_POST["to_delete"])) {
        $formData = $_POST["to_delete"];
        $product->delete($formData);
        echo "<meta http-equiv='refresh' content='0'>";
    } else {
        echo "please select the checkboxes";
    }
}
?>

<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.html">Scandiweb</a>
</nav>

<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <h2> Product List</h2>
        </div>
        <div class="col-lg-3">
            <form action="" method="POST">
                <input type="submit" name="delete_arr" class="delete-product-button" value="Mass Delete">
                <a id="button" href='add-product'>
                    Add
                </a>
        </div>
    </div>
    <hr class="line">
</div>

<div class="container main">
    <div class="row">
        <?php
        foreach ($product_data as $data) {
            ?>
            <div class="col-12 col-md-6 col-lg-4">

                <div class="card">
                    <input type='checkbox' name='to_delete[]' class='delete-checkbox' value='<?php
                    echo $data->sku ?>'>

                    <div class="card-body">

                        <b><a href="#" title="View Product"><?php
                                echo $data->sku ?></a>
                        </b>
                        <h4><a href="#" title="View Product"><?php
                                echo $data->name ?></a>
                        </h4>
                        <p><?php
                            echo $data->attributes ?></p>
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="btn btn-danger btn-block"><?php
                                    echo $data->price ?>$</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <div class="push"></div>
</div>
</form>

<footer>
    <div class="container">
        <hr class="line">
        <h4>Scandiweb Test assignment</h4>
    </div>
</footer>
</body>

</html>
