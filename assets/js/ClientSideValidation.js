$(document).ready(function () {
    $("#submitbtn").click(function (e) {
        var check = ValidateAll();
        return check;
    });
});

var errorCounter;
var errorMessage;
var errorCounter2;
var errorMessage2;

function ValidateAll() {
    $(".error").remove();
    errorCounter = 0;
    errorCounter2 = 0;

    var skuVal = $('#sku');
    var nameVal = $('#name');
    var productVal = $('#productType');
    var priceVal = $('#price');
    var attributeVal = productVal.val();

    sku_validation(skuVal);
    alphanumeric_validation(nameVal);
    alphanumeric_validation(productVal);
    decimal_validation(priceVal);

    //get all the input ids inside the div of selected productType (Furniture or Disk or Book)
    $("#" + attributeVal).find("input").each(function () {
        var attributeId = $('#' + this.id);
        decimal_validation(attributeId);
    })

    if (errorCounter == 0 && errorCounter2 == 0) {
        return true;
    } else {
        return false;
    }
}

function sku_validation(testVal) {
    //regex only allows number (mandatory) and alphabets (optional)
    var alphanumericReg = /^(?=.*[0-9].*)([a-zA-Z0-9]+)$/;
    var valCheck = testVal.val();
    if (valCheck === "") {
        testVal.after('<span class="error"> Please, submit required data.</span>');
        errorCounter = errorCounter + 1;
    } else if (!alphanumericReg.test(valCheck)) {

        testVal.after('<span class="error"> Please provide the data of indicated type.</span>');
        errorCounter = errorCounter + 1;
    }
}

function alphanumeric_validation(testVal) {
    //regex only allows alphabets (mandatory), number and spaces (optional).
    var nameReg = /^(?=.*[a-zA-Z].*)([a-zA-Z0-9\s]+)$/;
    var valCheck = testVal.val();
    if (valCheck === "") {
        testVal.after('<span class="error"> Please, submit required data.</span>');
        errorCounter = errorCounter + 1;
    } else if (!nameReg.test(valCheck)) {
        testVal.after('<span class="error"> Please provide the data of indicated type.</span>');
        errorCounter = errorCounter + 1;

    }
}

function decimal_validation(numVal) {
    //regex only allows numbers and maximum 4 digits after the decimal
    var numericReg = /^(\d*)\.?(\d){0,4}$/;
    var numCheck = numVal.val();
    if (numCheck === "") {
        numVal.after('<span class="error"> Please, submit required data.</span>');
        errorCounter = errorCounter + 1;
    }
    if (!numericReg.test(numCheck)) {
        numVal.after('<span class="error"> Please provide the data of indicated type.</span>');
        errorCounter2 = errorCounter2 + 1;
    }
}
