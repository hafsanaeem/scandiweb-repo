<?php
require "vendor/autoload.php";
?>
<!DOCTYPE html>
<html>

<head>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.0.min.js"></script>
    <link href="assets/css/style.css" rel="stylesheet">
    <title>Scandiweb Task</title>

</head>
<script type="text/javascript" src="assets/js/ClientSideValidation.js"></script>
<script type="text/javascript" lang="javascript">

    $(document).ready(function () {
        $("#productType").change(function () {
            $(this).find("option:selected").each(function () {
                var optionValue = $(this).attr("value");
                if (optionValue) {
                    $(".box").not("#" + optionValue).hide();
                    $("#" + optionValue).show();
                } else {
                    $(".box").hide();
                }
                $('.box').find('input:hidden').val('');
            });
        }).change();
    });

</script>
<?php
$errors = [];
if (isset($_POST['submit'])) {
    $validate = new ServerSideValidation($_POST);
    $errors = $validate->validateData();

    if (empty($errors)) {
        $_POST['Furniture'] = "Dimensions: ".(implode("X", $_POST['Furniture']));
        $_POST['Book'] = "Weight: ".$_POST['Book']['Weight']."KG";
        $_POST['Disk'] = "Size: ".$_POST['Disk']['Size']. "MB";

        $sendData = new TaskController();

        $checkOperation = $sendData->add($_POST);
        if ($checkOperation) {
            header("Location: index");
        }
        else{
            $duplicateSKU = "SKU already exists";
        }
    }
}
?>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.html">Scandiweb</a>
</nav>

    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <h2> Product Add</h2>
            </div>
            <div class="col-lg-3">
                <input type="submit" id="submitbtn" name="submit" form="product_form" class="btn btn-danger"
                       value="Add">
                <a class="btn btn-danger" href="index">Cancel</a>
            </div>
        </div>
        <hr class="line">
    </div>

    <div class="container main" id="form_container">

        <form action="<?php
        echo $_SERVER['PHP_SELF'] ?>" id="product_form" method="POST">
            <div>
                <label for="sku">SKU *</label>
                <input type="text" id="sku" name="sku" value="" required>
                <?php
                echo $errors['sku'] ?? '';
                echo $duplicateSKU ?? '' ?>
            </div>
            <div>
                <label for="name">Name *</label>
                <input type="text" id="name" name="name" value="" required>
                <?php
                echo $errors['name'] ?? '' ?>
            </div>

            <div>
                <label for="price">Price($) *</label>
                <input type="text" id="price" name="price" value="" required>
                <?php
                echo $errors['price'] ?? '' ?>
            </div>
            <div>
                <label for="typeSwitcher">Type Switcher *</label>
                <select id="productType" name="productType">
                    <option value="">Type Switcher</option>
                    <option value="Furniture">Furniture</option>
                    <option value="Book">Book</option>
                    <option value="Disk">Disk</option>
                </select>
                <?php
                echo $errors['productType'] ?? '' ?>
            </div>
            <div id="Furniture" class="box">
                <label for="Height">Height (CM) *</label>
                <input type="text" id="height" name="Furniture[Height]">
                <?php
                echo $errors['Height'] ?? '' ?>
                <label for="Width">Width (CM) *</label>
                <input type="text" id="width" name="Furniture[Width]">
                <?php
                echo $errors['Width'] ?? '' ?>
                <label for="Length">Length (CM) *</label>
                <input type="text" id="length" name="Furniture[Length]">
                <?php
                echo $errors['Length'] ?? '' ?>
                <p> Please provide dimensions</p>

            </div>
            <div id="Book" class="box">
                <label for="Weight">Weight (KG) *</label>
                <input type="text" id="weight" name="Book[Weight]">
                <p>Please, provide weight</p>
                <?php
                echo $errors['Weight'] ?? '' ?>

            </div>
            <div id="Disk" class="box">
                <label for="Size">Size (MB) *</label>
                <input type="text" id="size" name="Disk[Size]">
                <p>Please, provide size</p>
                <?php
                echo $errors['Size'] ?? '' ?>
            </div>
            <div class="error">
            </div>
        </form>
        <div class="push"></div>
    </div>

<footer id="footer" class "navbar-fixed-bottom" >
    <div class="container">
        <hr class="line">
        <h4>Scandiweb Test assignment</h4>
    </div>
</footer>
</body>
</html>

