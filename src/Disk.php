<?php

class Disk extends Products
{
    private $size;
    private array $errors = [];

    public function validateProduct($attributes)
    {
        $data = [];
        array_walk($attributes, function ($item, $key) use (&$data) {
            foreach ($item as $key => $value) {
                $data[$key] = $value;
            }
        });

        $this->size = $data["Size"];
        $this->validateSize();
        return $this->errors;

    }
    public function validateSize()
    {
        $val = trim($this->size);

        if (empty($val))
        {
            $this->addError( 'Size cannot be empty');
        }
        else
        {
            if (!preg_match('/^(\d*)\.?(\d){0,4}$/', $val))
            {
                $this->addError( 'Size must be numeric. Size must not exceed 4 characters after decimal');
            }
        }
    }
    private function addError($val)
    {
        $this->errors['Size'] = $val;
    }
}