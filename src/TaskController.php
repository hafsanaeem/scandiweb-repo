<?php

class TaskController extends TaskGateway

{
    protected $db_table = 'products';
    protected $column = 'sku';
    protected $db_table_fields = array('sku', 'name', 'price', 'type', 'attributes');

    public $sku;
    public $name;
    public $price;
    public $type;
    public $attributes;

    function __construct()
    {
        parent::__construct($this->db_table, $this->db_table_fields,$this->column);
    }

    public function getAll()
    {
        $new = $this->find_All();
        return $new;
    }

    public function add($productData)
    {

        $this->sku = $productData['sku'];
        $this->name = $productData['name'];
        $this->price = $productData['price'];
        $this->type = $productData['productType'];
        $this->attributes = $productData[$this->type];
        //check if SKU already exists and return error else add to database
        $checkSku = $this->find_by_id($this->sku);
        if ($checkSku){
            return false;
        }
        else{
         return  $this->create();
        }
    }

    public function delete($formData){

        $this->delete_data($formData);
    }
}
