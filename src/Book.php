<?php

class Book extends Products
{
    private $weight;
    private $errors = [];

    public function validateProduct($attributes)
    {
        $data = [];
        array_walk($attributes, function ($item, $key) use (&$data) {
            foreach ($item as $key => $value) {
                $data[$key] = $value;
            }
        });
        $this->weight = $data["Weight"];
        $this->validateWeight();
        return $this->errors;
    }

    public function validateWeight()
    {
        $val = trim($this->weight);
        if (empty($val)) {
            $this->addError('weight cannot be empty');
        } else {
            if (!preg_match('/^(\d*)\.?(\d){0,4}$/', $val)) {
                $this->addError('weight must be numeric. Weight  must not exceed 4 characters after decimal');
            }
        }
    }

    private function addError($val)
    {
        $this->errors['Weight'] = $val;
    }
}