<?php

class Furniture extends Products
{
    private $height;
    private $width;
    private $length;
    private $errors = [];


    public function validateProduct($attributes)
    {
        $data = [];
        array_walk($attributes, function ($item, $key) use (&$data) {
            foreach ($item as $key => $value) {
                $data[$key] = $value;
            }
        });

        $this->height = $data["Height"];
        $this->width = $data["Width"];
        $this->length = $data["Length"];

        $this->validateHeight();
        $this->validateWidth();
        $this->validateLength();

        return $this->errors;
    }

    public function validateHeight()
    {
        $val = trim($this->height);

        if (empty($val)) {
            $this->addError('Height', 'height cannot be empty');
        } else {
            if (!preg_match('/^(\d*)\.?(\d){0,4}$/', $val)) {
                $this->addError('Height', 'height must be numeric. Height must not exceed 4 characters after decimal');
            }
        }
    }

    public function validateWidth()
    {
        $val = trim($this->width);

        if (empty($val)) {
            $this->addError('Width', 'width cannot be empty');
        } else {
            if (!preg_match('/^(\d*)\.?(\d){0,4}$/', $val)) {
                $this->addError('Width', 'width must be numeric. Width  must not exceed 4 characters after decimal');
            }
        }
    }

    public function validateLength()
    {
        $val = trim($this->length);

        if (empty($val)) {
            $this->addError('Length', 'length cannot be empty');
        } else {
            if (!preg_match('/^(\d*)\.?(\d){0,4}$/', $val)) {
                $this->addError('Length', 'length must be numeric. Length must not exceed 4 characters after decimal');
            }
        }
    }

    private function addError($key, $val)
    {
        $this->errors[$key] = $val;
    }
}