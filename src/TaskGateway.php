<?php

abstract class TaskGateway
{
    private $conn;
    private $db_table;
    private $table_fields;
    private $id;
    private $column;

    function __construct($db_table, $db_table_fields, $column)
    {
        $this->conn = new Database();
        $this->db_table = $db_table;
        $this->table_fields = $db_table_fields;
        $this->column = $column;
    }

    public function find_all()
    {
        return self::find_by_query("SELECT * FROM " . $this->db_table . " ORDER BY " . $this->column . " ASC");
    }

    public function find_by_query($sql)
    {
        $result_set = $this->conn->query($sql);
        $the_object_array = array();
        while ($row = mysqli_fetch_array($result_set)) {
            $the_object_array[] = static::instantation($row);
        }
        return $the_object_array;
    }

    public function instantation($the_record)
    {
        $calling_class = get_called_class();
        $the_object = new $calling_class;
        foreach ($the_record as $the_attribute => $value) {
            if ($the_object->has_the_attribute($the_attribute)) {
                $the_object->$the_attribute = $value;
            }
        }
        return $the_object;
    }

    private function has_the_attribute($the_attribute)
    {
        return property_exists($this, $the_attribute);
    }

    public function clean_properties()
    {
        $clean_properties = array();
        foreach ($this->properties() as $key => $value) {
            $clean_properties[$key] = $this->conn->escape_string($value);
        }

        return $clean_properties;
    }

    public function find_by_id($value)
    {
        $the_result_array = self::find_by_query(
            "SELECT * FROM " . $this->db_table . " WHERE " . $this->column . " = '" . $value . "'"
        );

        if (!empty($the_result_array)) {
            return true;
        } else {
            return false;
        }
    }

    public function create()
    {
        $properties = $this->clean_properties();
        $sql = "INSERT INTO " . $this->db_table . "(" . implode(",", array_keys($properties)) . ")";
        $sql .= "VALUES ('" . implode("','", array_values($properties)) . "')";

        if ($this->conn->query($sql)) {
            $this->id = $this->conn->the_insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function properties()
    {
        $properties = array();

        foreach ($this->table_fields as $db_field) {
            if (property_exists($this, $db_field)) {
                $properties[$db_field] = $this->$db_field;
            }
        }

        return $properties;
    }

    public function delete_data($values)
    {
        foreach ($values as $delete_values) {
            $sql = "DELETE FROM  " . $this->db_table . " WHERE " . $this->column . " = '" . $delete_values . "'";
            $this->conn->query($sql);
        }
    }
}

