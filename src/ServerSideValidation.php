<?php

class ServerSideValidation
{
    private $data;
    private $errors = [];
    private static $fields = ['sku', 'name', 'price', 'productType'];

    public function __construct($postData)
    {
        $this->data = $postData;
    }

    public function validateData()
    {
        foreach (self::$fields as $field) {
            if (!array_key_exists($field, $this->data)) {
                trigger_error("'$field' is not present in the data");
                return;
            }
        }

        $this->validateSku();
        $this->validateName();
        $this->validatePrice();
        $this->validateProductAttributes();

        return $this->errors;
    }

    private function validateSku()
    {
        $val = trim($this->data['sku']);
        if (empty($val)) {
            $this->addError('sku', 'sku cannot be empty');
        } else {
            if (!preg_match('/^(?=.*[0-9].*)([a-zA-Z0-9]+)$/', $val)) {
                $this->addError('sku', 'sku must be alphanumeric');
            }
        }
    }

    private function validateName()
    {
        $val = trim($this->data['name']);
        if (empty($val)) {
            $this->addError('name', 'name cannot be empty');
        } else {
            if (!preg_match('/^(?=.*[a-zA-Z].*)([a-zA-Z0-9\s]+)$/', $val)) {
                $this->addError('name', 'name must contain alphabets');
            }
        }
    }

    private function validatePrice()
    {
        $val = trim($this->data['price']);
        if (empty($val)) {
            $this->addError('price', 'price cannot be empty');
        } else {
            if (!preg_match('/^(\d*)\.?(\d){0,4}$/', $val)) {
                $this->addError('price', 'price must be numeric and not exceed 4 characters after decimal');
            }
        }
    }

        private function validateProductAttributes()
    {
        $val = trim($this->data['productType']);
        if (empty($val)) {
            $this->addError('productType', 'Product type cannot be empty');
        } else {
            //creating an object of a child class based on the selected productType
            $productObj = new $val();
            //assigning the values to attributes of the selected productType
            $attributes = $_POST[$val];

            $attributesErrors = call_user_func(array($productObj, 'validateProduct'), array($attributes));
            if (!empty($attributesErrors)) {
                foreach ($attributesErrors as $errorKey => $errorVal) {
                    $this->addError($errorKey, $errorVal);
                }
            }
        }
    }

    private function addError($key, $val)
    {
        $this->errors[$key] = $val;
    }

}

