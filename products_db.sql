-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 29, 2022 at 06:26 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `products_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `sku` varchar(30) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(11,0) NOT NULL,
  `type` varchar(20) NOT NULL,
  `attributes` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`sku`, `name`, `price`, `type`, `attributes`) VALUES
('GGWP0007', 'Atomic Habits', '12', 'Book', 'Weight: 1KG'),
('GGWP0009', 'Power of Your Subconscious Mind', '16', 'Book', 'Weight: 2KG'),
('GGWP0011', 'Stillness Is the Key', '12', 'Book', 'Weight: 1.5KG'),
('JVC200121', 'Taylor Swift Album', '20', 'Disk', 'Size: 500MB'),
('JVC200123', 'Acme Disc', '12', 'Disk', 'Size: 600MB'),
('JVC200124', 'Music Disc', '20', 'Disk', 'Size: 900MB'),
('TR120555', 'Sofa ', '400', 'Furniture', 'Dimensions: 30X64X35'),
('TR130515', 'Cupboard', '201', 'Furniture', 'Dimensions: 90X45X85'),
('TR130545', 'Chair', '100', 'Furniture', 'Dimensions: 30X35X30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`sku`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
